# 🛠 fix-a-bug

## Setup

Clone this repo.

```bash
git clone https://github.com/datamade/fix-a-bug.git && cd fix-a-bug
```

Install the requirements. (Feel free to use a virtual environment!)

```bash
pip install -r requirements.txt
```

Run the tests.

```bash
pytest -sv
```

## Challenge

The code in `fix_me.py` contains four errors that are preventing the test from
passing. Debug and correct each error. As you identify the errors, fill out
a description of the problem and an explanation of the root cause and your fix
in the section below.

### Error 1

**Description:**
Class inheritance in the wrong order
**Explanation:**
In Python when a class inherents from two parents that both have the same fields, python defaults to using the parent listed first in the inheritance. The test makes it clear that it is expecting DATA to contain the array [4,5,6]. We could fix this a few ways, and ideally parent classes wouldn't have variables with the same name, but the one that seems like it would have minimum input to me is to switch the order of ParentA and ParentB

### Error 2

**Description:**
previous_value does not behave as described
**Explanation:**
The comment in previous_value specifies that if the current_index is at the beginning of the array, that the function should return 0. However, currently the function is requesting index -1 from the array "DATA" which in this case returns the last element of the array. Again, there are many possible ways to fix this, my choice was to create a new array that starts with 0 and ends with the original array and simply search for the current_index. I could forsee this possibly leading to
bugs in a more complex program, but for our purposes it should work just fine

### Error 3

**Description:**
enumerate is backwards
**Explanation:**
When using enumerate, the return value puts the index first and the value second. We just have to swap the order.

### Error 4

**Description:**
sum takes an array
**Explanation:**
When using the sum function, the values we want to sum should be in an array. We'll just add brackets to the values
